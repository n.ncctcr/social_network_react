import css from './Footer.module.css'

const Footer = () => {
    return (
        <div className={css.footer}>
            <a target="_blank" href="https://www.linkedin.com/in/nikolay-nesterchuk-290620186/">by ncctcr.</a>
        </div>
    )
}

export default Footer;
