import css from './Dialog.module.css'
import {NavLink} from "react-router-dom";

const Dialog = (props) => {
    let path = '/dialogs/' + props.id

    return  (
        <div>
            <NavLink className={css.dialog} to={path}>
                <div>
                    <img className={css.avatar} src={props.avatar} alt=""/>
                </div>
                <div className={css.dialogInfo}>
                    {props.name}
                    <div className={css.previewLastMessage}> {props.lastMsg}</div>
                </div>
            </NavLink>
        </div>
    )
}

export default Dialog
