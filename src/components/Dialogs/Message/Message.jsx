import css from './Message.module.css';

const Message = (props) => {
    return (
        <div className={css.messageRow}>
            <div className={css.messageBody}>{props.message}</div>
        </div>
    )
}

export default Message
