import css from './Dialogs.module.css';
import Dialog from "./Dialog/Dialog";
import {createRef} from "react";
import Message from "./Message/Message";

const Dialogs = (props) => {

    let newMessageElement = createRef()

    const onMessageChange = () => { props.updateNewMessageBody(newMessageElement.current.value) }
    const sendNewMessage = () => { props.sendMessage() }

    let dialogsElements = props.dialogsPage.dialogs
        .map( d => <Dialog key={d.id} id={d.id} name={d.name} lastMsg={d.lastMsg} avatar={d.avatar}/>)
    let messagesElement = props.dialogsPage.messages.map( m => <Message key={m.id} message={m.message}/>)

    return (
        <div className={css.dialogs}>
            <div className={css.dialogsItems}>
                { dialogsElements }
            </div>
            <div className={css.blockMessages}>
                <div className={css.listMessages}>
                    { messagesElement }
                </div>
                <div className={css.blockTypingMessages}>
                    <textarea className={css.newMessageTextarea}
                              ref={newMessageElement}
                              placeholder='Enter your message'
                              onChange={() => onMessageChange()}
                              value={props.dialogsPage.newMessageText}/>
                    <div>
                        <button className={css.sendButton}
                                onClick={() => { sendNewMessage() }}>send</button>
                    </div>
                </div>
            </div>
        </div>
    )
}

export default Dialogs
