import css from './Post.module.css'

const Post = (props) => {
    return (
        <div className={css.post}>
            <div className={css.generalBodyPost}>
                <img src="https://media.wired.com/photos/5cdefb92b86e041493d389df/125:94/w_1265,h_951,c_limit/Culture-Grumpy-Cat-487386121.jpg" alt=""/>
                <div className={css.messageContent}>
                    <div className={css.nameAndMessage}>
                        <div className={css.name}>Meowich Murchello</div>
                        <div className={css.nickname}>@meowizy</div>
                        <div className={css.nickname}>05.06 15:46</div>
                    </div>
                    <div>
                        <div className={css.message}>{props.message}</div>
                    </div>
                </div>

            </div>
            {/*<div>likes: {props.likes}</div>*/}
        </div>
    );
}

export default Post;
