import css from './MyPosts.module.css'
import Post from "./Post/Post";
import {createRef} from "react";

const MyPosts = (props) => {

    let postsElement = props.posts.map( p => (
        <Post key={p.id} message={p.message} likes={p.likes}/>
    )).reverse()

    let newPostElement = createRef()

    const onAddPost = () => {
        props.addPost()
    }

    const onPostChange = () => {
        props.updateNewPostText(newPostElement.current.value)
    }

    const handleKeyDown = (e) => {
        if (e.key === 'Enter' && e.ctrlKey) {
            props.addPost()
        }
    }

    return (
        <div className={css.postsBlock}>
            <div>
                <div className={css.newPostBlock}>
                    <textarea className={css.bodyNewMessage}
                              ref={newPostElement}
                              onKeyDown={handleKeyDown}
                              onChange={() => onPostChange()}
                              value={props.newPostText}/>
                    <div>
                        <button disabled={props.newPostText === ''}
                                title='Ctrl + Enter add new post'
                                className={css.buttonAddPost}
                                onClick={ onAddPost }>
                            <span>Add post</span>
                        </button>
                    </div>
                </div>
            </div>
            <div className={css.posts}>
                { postsElement }
            </div>
        </div>
    );
}

export default MyPosts;
