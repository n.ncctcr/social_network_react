import css from './ProfileInfo.module.css'

const ProfileInfo = () => {
    return (
        <div>
            <div>
                <div className={css.contentImage}/>
            </div>
            <div className={css.infoBlock}>
                <img className={css.avatar} src="https://media.wired.com/photos/5cdefb92b86e041493d389df/125:94/w_1265,h_951,c_limit/Culture-Grumpy-Cat-487386121.jpg" alt=""/>
                <div className={css.description}>
                    <h2 className={css.name}>Meowich Murchello <div className={css.itsU}>(it's you)</div></h2>
                    <div className={css.status}>It’s not the one who licked Valerianka, but the one who stole sour cream from the owner</div>
                </div>
            </div>
        </div>
    );
}

export default ProfileInfo;
