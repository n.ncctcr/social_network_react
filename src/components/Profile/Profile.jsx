import css from './Profile.module.css'
import ProfileInfo from "./ProfileInfo/ProfileInfo";
import {NavLink} from "react-router-dom";
import MyPostsContainer from "./MyPosts/MyPostsContainer";


const FriendItem = (props) => {
    return (
        <NavLink className={css.friendBody} to='/dialogs'>
            <img className={css.friendAvatar} src={props.avatar} alt=""/>
            <div className={css.friendName}>
                {props.name}
            </div>
        </NavLink>
    )
}

const Profile = (props) => {
    // let store = props.store.getState()
    // let friendsElement = store.profilePage.friends.map(f => <FriendItem avatar={f.avatar} name={f.name} id={f.id}/>)
    return (
        <div className={css.profileBlock}>
            <ProfileInfo />
            <div className={css.bottomBlock}>
                <div className={css.leftSideBlock}>
                   <div className={css.friendsBlock}>
                       <div className={css.friendsBlockTitle}>Friends (48)</div>
                       {/*<div className={css.friendsBlockList}>{ friendsElement }</div>*/}
                   </div>
                </div>
                <div className={css.postsBlock}>
                    <MyPostsContainer />
                </div>
            </div>
        </div>
    );
}

export default Profile;
