import css from './Navbar.module.css'
import {NavLink} from "react-router-dom";

const Navbar = () => {
    return (
        <nav className={css.nav}>
            <div className={css.block}>
                <div className={css.item}>
                    <NavLink to='/profile'>Profile</NavLink>
                </div>
                <div className={css.item}>
                    <NavLink to='/dialogs'>Dialogs</NavLink>
                </div>
                <div className={css.item}>
                    <NavLink to='/friends'>Friends</NavLink>
                </div>
                <div className={css.item}>
                    <NavLink to='/news'>News</NavLink>
                </div>
                <div className={css.item}>
                    <NavLink to='/music'>Music</NavLink>
                </div>
                <div className={css.item}>
                    <NavLink to='/settings'>Settings</NavLink>
                </div>
            </div>
        </nav>
    )
}

export default Navbar;
