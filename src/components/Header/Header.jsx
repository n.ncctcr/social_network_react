import css from './Header.module.css'

const Header = () => {
    return (
        <header className={css.header}>
            <div className={css.logo}>
                <img src="https://upload.wikimedia.org/wikipedia/commons/thumb/a/a7/React-icon.svg/1280px-React-icon.svg.png" alt=""/>
                <div>Meowbook</div>
            </div>
            <div className={css.exit}>exit</div>
        </header>
    )
}

export default Header;
