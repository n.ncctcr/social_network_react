import css from './Friends.module.css'
import Friend from "./Friend/Friend";

const Friends = (props) => {
    let listFriends = props.state.friends.map(f => <Friend key={f.id} name={f.name} avatar={f.avatar} id={f.id}/>)

    return (
        <div className={css.gridBlocks}>
            <div className={css.friendsBlock}>
                { listFriends }
            </div>
            <div className={css.settingsBlock}>Settings</div>
        </div>
    )
}

export default Friends
