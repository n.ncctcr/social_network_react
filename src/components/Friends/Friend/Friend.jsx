import {NavLink} from "react-router-dom";
import css from './Friend.module.css'

const Friend = (props) => {
    return (
        <div className={css.friendRow}>
            <div>
                <img className={css.avatar} src={props.avatar} alt=""/>
            </div>
            <div className={css.friendInfo}>
                <div className={css.name}>{props.name}</div>
                <NavLink to={'/dialogs/' + props.id}>to write a message</NavLink>
            </div>
        </div>
    )
}

export default Friend
