const UPDATE_NEW_MESSAGE_BODY = 'UPDATE-NEW-MESSAGE-BODY'
const SEND_MESSAGE = 'SEND-MESSAGE'

let initialState = {
    dialogs: [
        {
            id: 1,
            name: 'John Week',
            lastMsg: 'How are u?',
            avatar: 'http://images2.minutemediacdn.com/image/upload/c_crop,h_1193,w_2121,x_0,y_64/f_auto,q_auto,w_1100/v1565279671/shape/mentalfloss/578211-gettyimages-542930526.jpg'
        },
        {
            id: 2,
            name: 'Samuel Rout',
            lastMsg: 'What?',
            avatar: 'https://static01.nyt.com/images/2020/04/22/science/22VIRUS-PETCATS1/22VIRUS-PETCATS1-mediumSquareAt3X.jpg'
        },
        {
            id: 3,
            name: 'Andry Bryan',
            lastMsg: 'OMG....',
            avatar: 'https://i.natgeofe.com/n/9135ca87-0115-4a22-8caf-d1bdef97a814/75552.jpg?w=636&h=424'
        },
        {
            id: 4,
            name: 'No Name',
            lastMsg: 'One more time',
            avatar: 'https://scitechdaily.com/images/Cat-Wearing-COVID-19-Mask.jpg'
        },
        {
            id: 5,
            name: 'Carusel El',
            lastMsg: 'Step by step',
            avatar: 'https://encrypted-tbn0.gstatic.com/images?q=tbn:ANd9GcRNOYjDpyLTBU4psP_ecaHYUUtGqcvsREezkA&usqp=CAU'
        },
        {
            id: 6,
            name: 'Meow Kitty',
            lastMsg: 'WTF!!!',
            avatar: 'https://www.jacksongalaxy.com/wp-content/uploads/2018/11/aggression-in-cats.jpg'
        },
    ],
    messages: [
        {id: 1, message: 'hi'},
        {id: 2, message: 'Hello'},
        {id: 3, message: 'How are u?'},
        {id: 4, message: 'How are u?'},
        {id: 5, message: 'How are u?'},
        {id: 6, message: 'How are u?'},
        {id: 7, message: 'How are u?'},
        {id: 8, message: 'How are u?'},
        {id: 9, message: 'Fine'},
    ],
    newMessageText: '',
}

const dialogsReducer = (state = initialState, action) => {
    switch (action.type) {
        case SEND_MESSAGE: {
            if (state.newMessageText) {
                let newId = state.messages[state.messages.length - 1].id + 1
                return {
                    ...state,
                    newMessageText: '',
                    messages: [...state.messages, { id: newId, message: state.newMessageText }]
                }
            }
            break
        }
        case UPDATE_NEW_MESSAGE_BODY: {
            return {
                ...state,
                newMessageText: action.body
            }
        }
        default:
            break
    }
    return state
}

export const sendMessageCreator = () => ({ type: SEND_MESSAGE })
export const updateNewMessageBodyCreator = (body) => ({ type: UPDATE_NEW_MESSAGE_BODY, body: body })

export default dialogsReducer
