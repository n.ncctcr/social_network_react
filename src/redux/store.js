import profileReducer from "./profile-reducer";
import dialogsReducer from "./dialogs-reducer";
import friendsReducer from "./friends-reducer";

let store = {
    _state: {
        profilePage: {
            friends: [
                {
                    id: 1,
                    name: 'John Week',
                    avatar: 'http://images2.minutemediacdn.com/image/upload/c_crop,h_1193,w_2121,x_0,y_64/f_auto,q_auto,w_1100/v1565279671/shape/mentalfloss/578211-gettyimages-542930526.jpg'
                },
                {
                    id: 2,
                    name: 'Samuel Rout',
                    avatar: 'https://static01.nyt.com/images/2020/04/22/science/22VIRUS-PETCATS1/22VIRUS-PETCATS1-mediumSquareAt3X.jpg'
                },
                {
                    id: 3,
                    name: 'Andry Bryan',
                    avatar: 'https://i.natgeofe.com/n/9135ca87-0115-4a22-8caf-d1bdef97a814/75552.jpg?w=636&h=424'
                },
                {
                    id: 4,
                    name: 'No Name',
                    avatar: 'https://scitechdaily.com/images/Cat-Wearing-COVID-19-Mask.jpg'
                },
                {
                    id: 5,
                    name: 'Carusel El',
                    avatar: 'https://encrypted-tbn0.gstatic.com/images?q=tbn:ANd9GcRNOYjDpyLTBU4psP_ecaHYUUtGqcvsREezkA&usqp=CAU'
                },
                {
                    id: 6,
                    name: 'Meow Kitty',
                    avatar: 'https://www.jacksongalaxy.com/wp-content/uploads/2018/11/aggression-in-cats.jpg'
                },
            ],
            posts: [
                {id: 1, message: 'hi', likes: 5},
                {id: 2, message: 'Why i sad', likes: 4},
                {id: 3, message: 'Hello there!!', likes: 5},
                {id: 4, message: 'Its my first post', likes: 1},
                {id: 5, message: 'Why are u rooning??', likes: 1},
                {id: 6, message: 'Ha-ha its fun', likes: 1},
                {id: 7, message: 'Maybe baby', likes: 1},
            ],
            newPostText: '',
        },
        dialogsPage: {
            dialogs: [
                {
                    id: 1,
                    name: 'John Week',
                    lastMsg: 'How are u?',
                    avatar: 'http://images2.minutemediacdn.com/image/upload/c_crop,h_1193,w_2121,x_0,y_64/f_auto,q_auto,w_1100/v1565279671/shape/mentalfloss/578211-gettyimages-542930526.jpg'
                },
                {
                    id: 2,
                    name: 'Samuel Rout',
                    lastMsg: 'What?',
                    avatar: 'https://static01.nyt.com/images/2020/04/22/science/22VIRUS-PETCATS1/22VIRUS-PETCATS1-mediumSquareAt3X.jpg'
                },
                {
                    id: 3,
                    name: 'Andry Bryan',
                    lastMsg: 'OMG....',
                    avatar: 'https://i.natgeofe.com/n/9135ca87-0115-4a22-8caf-d1bdef97a814/75552.jpg?w=636&h=424'
                },
                {
                    id: 4,
                    name: 'No Name',
                    lastMsg: 'One more time',
                    avatar: 'https://scitechdaily.com/images/Cat-Wearing-COVID-19-Mask.jpg'
                },
                {
                    id: 5,
                    name: 'Carusel El',
                    lastMsg: 'Step by step',
                    avatar: 'https://encrypted-tbn0.gstatic.com/images?q=tbn:ANd9GcRNOYjDpyLTBU4psP_ecaHYUUtGqcvsREezkA&usqp=CAU'
                },
                {
                    id: 6,
                    name: 'Meow Kitty',
                    lastMsg: 'WTF!!!',
                    avatar: 'https://www.jacksongalaxy.com/wp-content/uploads/2018/11/aggression-in-cats.jpg'
                },
            ],
            messages: [
                {id: 1, message: 'hi'},
                {id: 2, message: 'Hello'},
                {id: 3, message: 'How are u?'},
                {id: 4, message: 'How are u?'},
                {id: 5, message: 'How are u?'},
                {id: 6, message: 'How are u?'},
                {id: 7, message: 'How are u?'},
                {id: 8, message: 'How are u?'},
                {id: 9, message: 'Fine'},
            ],
            newMessageText: '',
        },
        friendsPage: {
            friends: [
                {
                    id: 1,
                    name: 'John Week',
                    avatar: 'http://images2.minutemediacdn.com/image/upload/c_crop,h_1193,w_2121,x_0,y_64/f_auto,q_auto,w_1100/v1565279671/shape/mentalfloss/578211-gettyimages-542930526.jpg'
                },
                {
                    id: 2,
                    name: 'Samuel Rout',
                    avatar: 'https://static01.nyt.com/images/2020/04/22/science/22VIRUS-PETCATS1/22VIRUS-PETCATS1-mediumSquareAt3X.jpg'
                },
                {
                    id: 3,
                    name: 'Andry Bryan',
                    avatar: 'https://i.natgeofe.com/n/9135ca87-0115-4a22-8caf-d1bdef97a814/75552.jpg?w=636&h=424'
                },
                {
                    id: 4,
                    name: 'No Name',
                    avatar: 'https://scitechdaily.com/images/Cat-Wearing-COVID-19-Mask.jpg'
                },
                {
                    id: 5,
                    name: 'Carusel El',
                    avatar: 'https://encrypted-tbn0.gstatic.com/images?q=tbn:ANd9GcRNOYjDpyLTBU4psP_ecaHYUUtGqcvsREezkA&usqp=CAU'
                },
                {
                    id: 6,
                    name: 'Meow Kitty',
                    avatar: 'https://www.jacksongalaxy.com/wp-content/uploads/2018/11/aggression-in-cats.jpg'
                },
            ]
        }
    },
    _callSubscriber (state) {
        console.log(state)
    },

    getState() {
        return this._state
    },
    subscribe(observer) {
        this._callSubscriber = observer;
    },

    dispatch(action) {
        this._state.profilePage = profileReducer(this._state.profilePage, action)
        this._state.dialogsPage = dialogsReducer(this._state.dialogsPage, action)
        this._state.friendsPage = friendsReducer(this._state.friendsPage, action)
        this._callSubscriber(this._state)
    },
}

export default store

